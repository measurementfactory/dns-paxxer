#include <DnsPaxxer/DnsPaxxer.h>
#include <PcapWrp.h>
#include <xstd/TextException.h>
#include <iostream>
#include <cassert>
#include <cstring>
#include <cstdlib>

static
void tryPacket(const std::string &rawImage) {

	char srcBuf[2048]; // we supppose we deal also with TCP packets that is more than 512 bytes
	char dstBuf[2048];

	DnsPaxxer::Packer srcPacker(srcBuf, sizeof(srcBuf));
	DnsPaxxer::Packer dstPacker(dstBuf, sizeof(dstBuf));

	DnsPaxxer::Packet srcpkt(rawImage.c_str(), rawImage.length());
	srcpkt.pack(srcPacker);
	const DnsPaxxer::Packet::size_type srcBufSz = srcPacker.offset();
	srcpkt.print(cout<< "SRC:\n");

	DnsPaxxer::Packet dstpkt(srcBuf, srcBufSz);
	dstpkt.pack(dstPacker);
	const DnsPaxxer::Packet::size_type dstBufSz = dstPacker.offset();

	dstpkt.print(cout<<"DST:\n");

	assert(srcBufSz == dstBufSz);
	assert(0 == memcmp(srcBuf, dstBuf, srcBufSz));
}

static const char *ctxFile = 0;
static int ctxPacket = 0;
static int Errors = 0;

// TODO: place in separate file to share with other test cases
void TestPacket(const std::string &rawImage) {
	try {
		tryPacket(rawImage);
	}
	catch (const TextException &te) {
		++Errors;
		cerr << ctxFile << ':' << ctxPacket << ": error: " << te << endl;
	}
}

void testPcap(const char *fileName) {
	ctxFile = fileName;
	ctxPacket = 0;

	PcapWrp ps;
	ps.open(fileName);

	cout << ctxFile << ": found " << ps.packets().size() << " packets" << endl;
	typedef RawPackets::const_iterator RPCI;
	for (RPCI it = ps.packets().begin(); it != ps.packets().end(); ++it) {
		++ctxPacket; // like lines, packet numbers start with 1
		(*it)->print(cout << ctxFile << ':' << ctxPacket << ": ");
		TestPacket(std::string((char*)(*it)->buf, (*it)->len));
	}

	ps.close();
	ctxFile = 0;
}

int main(int argc, char * argv[]) {

	assert(argc > 1);
	try {
		for(int nArg=1; nArg<argc; ++nArg)
			testPcap(argv[nArg]);
	}
	catch(const TextException &te) {
		++Errors;
		cerr << ctxFile << ':' << ctxPacket << ": error: " << te << endl;
	}
	catch(const char *s) { // TODO: remove as unused?
		++Errors;
		cerr << ctxFile << ':' << ctxPacket << ": error: " << s << endl;
	}

	exit(Errors ? -1 : 0);
}
