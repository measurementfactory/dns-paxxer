#include "PcapWrp.h"

void _process_packet(u_char * user, const struct pcap_pkthdr * hdr, const u_char * packet) {
	PcapWrp * ps = (PcapWrp *)user;
	ps->process_packet(hdr, packet);
}
