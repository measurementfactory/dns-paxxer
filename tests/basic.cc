#include <DnsPaxxer/DnsPaxxer.h>
#include <xstd/Assert.h>
#include <xstd/TextException.h>
#include <iostream>
#include <iomanip>
#include <cassert>
#include <cstring>

const char pcap [] =
     "\324\303\262\241\002\000\004\000\000\000\000\000\000\000\000\000"
     "\377\377\000\000\001\000\000\000\042\053\116\106\205\333\014\000"
     "\112\000\000\000\112\000\000\000\000\000\044\300\015\044\000\026"
     "\166\071\004\233\010\000\105\000\000\074\006\206\000\000\100\021"
     "\020\344\014\240\045\005\014\240\045\003\357\016\000\065\000\050"
     "\141\174"

     // DNS QUERY MESSAGE STARTS
             "\274\356\001\000\000\001\000\000\000\000\000\000\003\167"
     "\167\167\006\147\157\157\147\154\145\003\143\157\155\000\000\001"
     "\000\001"
     // DNS QUERY MESSAGE ENDS

             "\042\053\116\106\042\374\014\000\176\001\000\000\176\001"
     "\000\000\000\026\166\071\004\233\000\000\044\300\015\044\010\000"
     "\105\000\001\160\305\146\000\000\100\021\120\317\014\240\045\003"
     "\014\240\045\005\000\065\357\016\001\134\056\146"

     // DNS REPLY MESSAGE BEGINS
                                                     "\274\356\201\200"
     "\000\001\000\005\000\007\000\007\003\167\167\167\006\147\157\157"
     "\147\154\145\003\143\157\155\000\000\001\000\001\300\014\000\005"
     "\000\001\000\001\147\336\000\010\003\167\167\167\001\154\300\020"
     "\300\054\000\001\000\001\000\000\000\030\000\004\102\371\131\150"
     "\300\054\000\001\000\001\000\000\000\030\000\004\102\371\131\223"
     "\300\054\000\001\000\001\000\000\000\030\000\004\102\371\131\143"
     "\300\054\000\001\000\001\000\000\000\030\000\004\102\371\131\147"
     "\300\060\000\002\000\001\000\000\370\155\000\004\001\142\300\060"
     "\300\060\000\002\000\001\000\000\370\155\000\004\001\143\300\060"
     "\300\060\000\002\000\001\000\000\370\155\000\004\001\144\300\060"
     "\300\060\000\002\000\001\000\000\370\155\000\004\001\145\300\060"
     "\300\060\000\002\000\001\000\000\370\155\000\004\001\146\300\060"
     "\300\060\000\002\000\001\000\000\370\155\000\004\001\147\300\060"
     "\300\060\000\002\000\001\000\000\370\155\000\004\001\141\300\060"
     "\300\340\000\001\000\001\000\000\027\054\000\004\321\125\213\011"
     "\300\200\000\001\000\001\000\000\027\054\000\004\100\351\263\011"
     "\300\220\000\001\000\001\000\001\041\055\000\004\100\351\241\011"
     "\300\240\000\001\000\001\000\001\013\265\000\004\102\371\135\011"
     "\300\260\000\001\000\001\000\000\027\054\000\004\321\125\211\011"
     "\300\300\000\001\000\001\000\000\027\054\000\004\110\016\353\011"
     "\300\320\000\001\000\001\000\000\152\377\000\004\100\351\247\011";
     // DNS REPLY MESSAGE ENDS

#if PCAP_TCPDUMP_OUTPUT
16:39:30.842629 IP 12.160.37.5.61198 > 12.160.37.3.53:  48366+ A? www.google.com. (32)
16:39:30.850978 IP 12.160.37.3.53 > 12.160.37.5.61198:  48366 5/7/7 CNAME www.l.google.com.,
 	A 66.249.89.104, A 66.249.89.147, A 66.249.89.99, A 66.249.89.103 (340)
#endif

#if PCAP_BASE64_ENCODED
1MOyoQIABAAAAAAAAAAAAP//AAABAAAAIitORoXbDABKAAAASgAAAAAAJMANJAAWdjkEmwgARQAA
PAaGAABAERDkDKAlBQygJQPvDgA1AChhfLzuAQAAAQAAAAAAAAN3d3cGZ29vZ2xlA2NvbQAAAQAB
IitORiL8DAB+AQAAfgEAAAAWdjkEmwAAJMANJAgARQABcMVmAABAEVDPDKAlAwygJQUANe8OAVwu
ZrzugYAAAQAFAAcABwN3d3cGZ29vZ2xlA2NvbQAAAQABwAwABQABAAFn3gAIA3d3dwFswBDALAAB
AAEAAAAYAARC+VlowCwAAQABAAAAGAAEQvlZk8AsAAEAAQAAABgABEL5WWPALAABAAEAAAAYAARC
+VlnwDAAAgABAAD4bQAEAWLAMMAwAAIAAQAA+G0ABAFjwDDAMAACAAEAAPhtAAQBZMAwwDAAAgAB
AAD4bQAEAWXAMMAwAAIAAQAA+G0ABAFmwDDAMAACAAEAAPhtAAQBZ8AwwDAAAgABAAD4bQAEAWHA
MMDgAAEAAQAAFywABNFViwnAgAABAAEAABcsAARA6bMJwJAAAQABAAEhLQAEQOmhCcCgAAEAAQAB
C7UABEL5XQnAsAABAAEAABcsAATRVYkJwMAAAQABAAAXLAAESA7rCcDQAAEAAQAAav8ABEDppwk=
#endif

using std::cout;
using std::endl;

void testQuery(const std::string &query) {
 	const DnsPaxxer::Packet pkt(query.data(), query.size());

 	const DnsPaxxer::Header &hdr = pkt.header();
 	assert(hdr.qr() == 0);	// make sure it is a query, not a reply
 	cout << "query ID: " << hdr.id() << endl;

 	// some kind of loop over Question RRs
	typedef DnsPaxxer::RRs::const_iterator RRI;
 	for (RRI i = pkt.questions().begin(); i != pkt.questions().end(); ++i) {
 		const DnsPaxxer::RR *rr = *i;
 		cout << "  qname: " << rr->name() << ", type: " << rr->type() << endl;
 	}
}

void testReply(const std::string &reply) {
 	const DnsPaxxer::Packet pkt(reply.data(), reply.size());

 	const DnsPaxxer::Header &hdr = pkt.header();
 	assert(hdr.qr() == 1);	// make sure it is a reply, not a query
 	cout << "response ID: " << hdr.id() << endl;

 	// some kind of loop over Answer RRs
	typedef DnsPaxxer::RRs::const_iterator RRI;
 	for (RRI i = pkt.answers().begin(); i != pkt.answers().end(); ++i)
 		cout << **i;
	cout << endl;

 	// another kind of loop over Answer RRs
 	for (RRI i = pkt.answers().begin(); i != pkt.answers().end(); ++i) {
 		DnsPaxxer::RR *rr = *i;
 		if (rr->type() == DnsPaxxer::RR::T_A)
			cout << rr->name() << " has IPv4 address " << rr->address() << endl;
 	}
}

//XXX: may be remove this function cause the automatic test testPcap() is already implemented
void testPackNParse(const std::string & query) {
	char buf1[1024];
	char buf2[1024];

	DnsPaxxer::Packer packer1(buf1, sizeof(buf1));
	DnsPaxxer::Packer packer2(buf2, sizeof(buf2));

	// parse source buffer
	DnsPaxxer::Packet srcpkt(query.data(), query.size());
	srcpkt.print(cout);

	// pack into another (first) buffer
	srcpkt.pack(packer1);
	const DnsPaxxer::Packet::size_type buf1sz = packer1.offset();

	// parse first buffer
	DnsPaxxer::Packet dstpkt(buf1, buf1sz);
	dstpkt.print(cout);

	// pack into second buffer
	srcpkt.pack(packer2);
	const DnsPaxxer::Packet::size_type buf2sz = packer2.offset();

	// compare first and second buffers
	assert(buf1sz == buf2sz);
	assert(0==memcmp(buf1,buf2, buf1sz));
}

void testSetQueryId(const std::string & query) {
	DnsPaxxer::Packet pkt(query.data(), query.size());
	const uint16_t oldQueryId = pkt.header().id();

	char buf[1024];
	DnsPaxxer::Packer packer(buf, sizeof(buf));
	pkt.pack(packer);

	const uint16_t newQueryId = oldQueryId + 1;
	DnsPaxxer::SetQueryId(buf, sizeof(buf), newQueryId);

	DnsPaxxer::Parser parser(buf, sizeof(buf));
	pkt.parse(parser);
	Must(pkt.header().id() == newQueryId);

	try {
		// this should fail because buf size is too small
		DnsPaxxer::SetQueryId(buf, 1, newQueryId);
		Must(false);
	}
	catch (const TextException &) {
	}
}

int main(int argc, char * argv[]) {
	try {
		testQuery(std::string(pcap + 82,  sizeof(pcap)-82-1 ));
		testReply(std::string(pcap + 172, sizeof(pcap)-172-1));
		testPackNParse(std::string(pcap + 82,  sizeof(pcap)-82-1 ));
		testPackNParse(std::string(pcap + 172, sizeof(pcap)-172-1));
		testSetQueryId(std::string(pcap + 82,  sizeof(pcap) - 82 - 1));
	}
	catch (const TextException &te) {
		cerr << te << endl;
		return -1;
	}

	return 0;
}
