#include <DnsPaxxer/DnsPaxxer.h>
#include <xstd/TextException.h>
#include <iostream>
#include <iomanip>

using std::cout;
using std::endl;

int main(int argc, char * argv[]) {
	DnsPaxxer::Packet *pkt = new DnsPaxxer::Packet;

	try {
		pkt->parse("\000\000\000\000\000\000\000\000", 0);
	}
	catch(TextException te) {
		cerr << te << endl;
	}
	pkt->print(cout);

	return 0;
}
