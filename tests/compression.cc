
#include <DnsPaxxer/DnsPaxxer.h>
#include <xstd/TextException.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cassert>
#include <cstring>

//
// To make packet strings below:
//
// tcpdump -n -r root-servers-net.pcap -c 1 -w root-servers-net.query
// od -b < root-servers-net.query | cut -c 10- | sed -e 's/ /\\/g' -e s'/^/"/' -e 's/$/"/'
// tcpdump -n -r root-servers-net.pcap -w root-servers-net.reply src port domain
// od -b < root-servers-net.reply | cut -c 10- | sed -e 's/ /\\/g' -e s'/^/"/' -e 's/$/"/'
//

const char query_pcap[] =
	"\324\303\262\241\002\000\004\000\000\000\000\000\000\000\000\000"
	"\377\377\000\000\001\000\000\000\320\245\140\112\025\320\000\000"
	"\127\000\000\000\127\000\000\000\000\000\044\300\014\331\000\021"
	"\021\107\141\306\010\000\105\000\000\111\251\313\000\000\100\021"
	"\274\276\012\000\000\032\012\000\000\001\301\164\000\065\000\065"
	"\024\141\061\116\001\000\000\001\000\000\000\000\000\001\014\162"
	"\157\157\164\055\163\145\162\166\145\162\163\003\156\145\164\000"
	"\000\377\000\001\000\000\051\017\377\000\000\000\000\000\000"
	;

const char reply_pcap[] =
	"\324\303\262\241\002\000\004\000\000\000\000\000\000\000\000\000"
	"\377\377\000\000\001\000\000\000\320\245\140\112\233\364\000\000"
	"\053\001\000\000\053\001\000\000\000\021\021\107\141\306\000\000"
	"\044\300\014\331\010\000\105\000\001\035\302\122\000\000\100\021"
	"\243\143\012\000\000\001\012\000\000\032\000\065\301\164\001\011"
	"\026\177\061\116\201\200\000\001\000\004\000\004\000\006\014\162"
	"\157\157\164\055\163\145\162\166\145\162\163\003\156\145\164\000"
	"\000\377\000\001\300\014\000\002\000\001\000\004\151\065\000\004"
	"\001\152\300\014\300\014\000\002\000\001\000\004\151\065\000\004"
	"\001\153\300\014\300\014\000\002\000\001\000\004\151\065\000\004"
	"\001\146\300\014\300\014\000\002\000\001\000\004\151\065\000\004"
	"\001\141\300\014\300\014\000\002\000\001\000\004\151\065\000\002"
	"\300\136\300\014\000\002\000\001\000\004\151\065\000\002\300\076"
	"\300\014\000\002\000\001\000\004\151\065\000\002\300\056\300\014"
	"\000\002\000\001\000\004\151\065\000\002\300\116\300\056\000\001"
	"\000\001\000\006\245\166\000\004\300\072\200\036\300\056\000\034"
	"\000\001\000\006\245\166\000\020\040\001\005\003\014\047\000\000"
	"\000\000\000\000\000\002\000\060\300\116\000\001\000\001\000\006"
	"\245\166\000\004\300\005\005\361\300\136\000\001\000\001\000\004"
	"\151\170\000\004\306\051\000\004\300\076\000\001\000\001\000\004"
	"\151\267\000\004\301\000\016\201\000\000\051\020\000\000\000\000"
	"\000\000\000"
	;

using std::cout;
using std::endl;

void testQuery(const std::string &query) {
	const DnsPaxxer::Packet pkt(query.data(), query.size());

	const DnsPaxxer::Header &hdr = pkt.header();
	assert(hdr.qr() == 0);  // make sure it is a query, not a reply
	cout << "query ID: " << hdr.id() << endl;

	// some kind of loop over Question RRs
	typedef DnsPaxxer::RRs::const_iterator RRI;
	for (RRI i = pkt.questions().begin(); i != pkt.questions().end(); ++i) {
			const DnsPaxxer::RR *rr = *i;
			cout << "  qname: " << rr->name() << ", type: " << rr->type() << endl;
	}
}

void testReply(const std::string &reply) {
	const DnsPaxxer::Packet pkt(reply.data(), reply.size());

	const DnsPaxxer::Header &hdr = pkt.header();
	assert(hdr.qr() == 1);  // make sure it is a reply, not a query
	cout << "response ID: " << hdr.id() << endl;

	// some kind of loop over Answer RRs
	typedef DnsPaxxer::RRs::const_iterator RRI;
	for (RRI i = pkt.answers().begin(); i != pkt.answers().end(); ++i) {
			DnsPaxxer::RR *rr = *i;
			rr->print(cout);
	}

	// another kind of loop over Answer RRs
	for (RRI i = pkt.answers().begin(); i != pkt.answers().end(); ++i) {
			DnsPaxxer::RR *rr = *i;
			if (rr->type() == DnsPaxxer::RR::T_A)
					cout << rr->name() << " has IPv4 address " << rr->address() << endl;
	}
}

void testParseNPack(const std::string & msg) {
	char buf[4096];

	// parse input buffer
	DnsPaxxer::Packet pkt(msg.data(), msg.size());
	ostringstream s;
	s << pkt; // remember the image
	cout << endl << "PARSED MESSAGE" << endl << s.str();

	// pack into another buffer
	DnsPaxxer::Packer packer(buf, sizeof(buf));
	pkt.pack(packer);
	const DnsPaxxer::Packet::size_type bufsz = packer.offset();

	DnsPaxxer::Packet pkt2(buf, bufsz);
	ostringstream s2;
	s2 << pkt2; // remember the image
	cout << endl << "REPARSED MESSAGE" << endl << s2.str();

	assert(s.str() == s2.str());

	// compare input and packed buffers
	assert(bufsz == msg.size());
	assert(0 == memcmp(msg.data(), buf, bufsz));
}

int main(int argc, char * argv[]) {
	try {
		testQuery(std::string(query_pcap + 82, sizeof(query_pcap)-82-1));
		testReply(std::string(reply_pcap + 82, sizeof(reply_pcap)-82-1));
		testParseNPack(std::string(query_pcap + 82, sizeof(query_pcap)-82-1));
		testParseNPack(std::string(reply_pcap + 82, sizeof(reply_pcap)-82-1));
	}
	catch (const TextException &te) {
		cerr << te << endl;
		return 1;
	}

	return 0;
}
