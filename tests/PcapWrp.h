#include <pcap.h>
#include <errno.h>
#include <time.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define __FAVOR_BSD
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <netinet/if_ether.h>
#include <cstring>

#include <iostream>
#include <string>
#include <vector>

#ifndef SOL_UDP
	#define SOL_UDP 17
#endif
#ifndef SOL_TCP
	#define SOL_TCP 6
#endif

using namespace std;

void _process_packet(u_char * user, const struct pcap_pkthdr * hdr, const u_char * packet);

//XXX: update coding style and properties
//XXX: maybe remain just raw buffer
class RawPacket {
	public:
		//XXX: maybe add initialization c-tor
		//RawPacket(time_t aTs, u_int len): ts(aTs) {}
		void print(ostream & os) {
			os	<< "(" << len << ") "
				<< inet_ntoa(src) << ":" << ntohs(srcport) << " "
				<< inet_ntoa(dst) << ":" << ntohs(dstport) << " "
				<< ctime(&ts);
		}

	public:
		time_t ts;		// time snapshot
		u_int len;		// packet length
		in_addr src;	// source ip
		in_addr dst;	// dest ip
		u_int srcport;	// source port
		u_int dstport;	// dst port
		u_char* buf;	// raw packet data
};
typedef vector<RawPacket*> RawPackets;

class PcapWrp {
	public:
		friend void _process_packet(u_char * user, const struct pcap_pkthdr * hdr, const u_char * packet);

	public:
		PcapWrp(): theSession(0) {}
		virtual ~PcapWrp() {
			close();
		}

		void open(string fname) {
			if(theSession) close();
			theSession = pcap_open_offline(fname.c_str(), errbuf);
			if(!theSession) throw errbuf;

			pcap_dispatch(theSession, (int)-1, _process_packet, (u_char*)this);
		}

		void close() {
			clear();
			if(theSession)
				pcap_close(theSession);
			theSession=0;
		}

		RawPackets & packets() {
			return theRawPackets;
		}

	protected:
		void process_packet(const struct pcap_pkthdr * hdr, const u_char * packet) {
			const ether_header *eth_hdr = (const ether_header *)packet;
			ip *ip_hdr;
			udphdr *udp_hdr;

			const u_int ETH_H = sizeof(ether_header);
			const u_int IP_H = sizeof(ip);
			const u_int UDP_H = sizeof(udphdr);
			//const u_int TCP_H = sizeof(tcphdr);

			if(hdr->len < ETH_H) {
				cout << "Incorrect packet size" << endl;
				return; //XXX: throw Texc
			}

			if(ntohs(eth_hdr->ether_type) != ETHERTYPE_IP) {
				cout << "Non-IP packet ( type=" << eth_hdr->ether_type << ", expected " << ETHERTYPE_IP << ")" << endl;
				return; //XXX: throw Texc
			}

			// skip non-UDP and non-TCP data
			ip_hdr = (ip*)(packet + ETH_H);
			if( (ip_hdr->ip_p != IPPROTO_UDP) && (ip_hdr->ip_p != IPPROTO_TCP)) {
				cout << "Non-UDP and Non-TCP packet" << endl;
				return; //XXX: throw Texc
			}

			// obtain TCP/UDP buffer
			RawPacket *p = new RawPacket;
			udp_hdr	= (udphdr*)(packet + ETH_H + IP_H);

			const u_int offset = ETH_H + IP_H +
				/*(ip_hdr->ip_p==SOL_UDP)?*/ UDP_H /*:TCP_H*/;
			p->len = hdr->len - offset;
			p->ts = hdr->ts.tv_sec;
			p->src = ip_hdr->ip_src;
			p->dst = ip_hdr->ip_dst;
			p->srcport = (ip_hdr->ip_p == SOL_UDP) ? udp_hdr->uh_sport : 0;
			p->dstport = (ip_hdr->ip_p == SOL_UDP) ? udp_hdr->uh_dport : 0;
			p->buf = new u_char[p->len];
			memcpy(p->buf, (char*)(packet + offset), p->len);

			theRawPackets.push_back(p);
		}

		void clear() {
			RawPackets::iterator it;
			for(it=theRawPackets.begin(); it!=theRawPackets.end(); ++it) {
				RawPacket* rp = *it;
				delete [] rp->buf;
				delete rp;
			}
			theRawPackets.clear();
		}

	private:
		pcap_t * theSession;
		char errbuf[PCAP_ERRBUF_SIZE];
		RawPackets theRawPackets;
};
