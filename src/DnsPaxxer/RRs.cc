#include <DnsPaxxer/Packet.h>
#include <DnsPaxxer/Packer.h>
#include <DnsPaxxer/Parser.h>
#include <DnsPaxxer/EDNS.h>
#include <DnsPaxxer/RRs.h>
#include <DnsPaxxer/RR.h>
#include <xstd/Assert.h>
#include <memory>

DnsPaxxer::RRs::RRs() {
}

DnsPaxxer::RRs::RRs(const RRs & rrs) {
	operator=(rrs); //XXX: maybe add protected copy() method and call it here and in operator=
}

DnsPaxxer::RRs& DnsPaxxer::RRs::operator = (const RRs & rrs) {
	if (this == &rrs)
		return *this;

	clear();
	reserve(rrs.size());

	for (const_iterator it = rrs.begin(); it != rrs.end(); ++it)
		push_back(new RR(**it));

	return *this;
}

void DnsPaxxer::RRs::pack(Packer & packer, bool bQuery) const {
	for (const_iterator it = begin(); it != end(); ++it)
		(*it)->pack(packer, bQuery);
}

void DnsPaxxer::RRs::parse(Parser & parser, uint16_t sCount, bool bQuery) {
	Must(empty()); // otherwise, we risk freeing RR that somebody uses

	reserve(sCount);

	while (sCount-- > 0) {
		std::auto_ptr<RR> record(new RR);
		record->parse(parser, bQuery);
		push_back(record.release());
	}
}

void DnsPaxxer::RRs::clear() {
	for(iterator it = begin(); it != end(); ++it)
		delete *it;
	std::vector<RR*>::clear();
}

std::ostream& DnsPaxxer::RRs::print(std::ostream& os) const {
	for (const_iterator it = begin(); it != end(); ++it)
		(*it)->print(os);
	return os;
}
