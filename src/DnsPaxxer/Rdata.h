#ifndef DNSPAXXER_RDATA_H
#define DNSPAXXER_RDATA_H

#include <DnsPaxxer/common.h>

class IBinStream;
class OBinStream;

namespace DnsPaxxer {

class Packet;
class Packer;
class Parser;
class RR;

// DNS Resource Rdata class
class Rdata {
	public:
		Rdata();
		virtual ~Rdata();
		virtual Rdata *clone() const = 0;
		virtual void clear() = 0;

		// textual representation in a zone file or in dig output
		std::string string() const;
		virtual void print(std::ostream & os) const = 0;

		virtual void pack(Packer & packer) const = 0;
		virtual void parse(Parser &parser) = 0;
};

class RawRdata: public Rdata {
	public:
		RawRdata();
		RawRdata(const std::string &raw);
		virtual RawRdata *clone() const { return new RawRdata(*this); }

		virtual void clear();
		virtual void print(std::ostream & os) const;

		virtual void pack(Packer & packer) const;
		virtual void parse(Parser & parser);

	protected:
		std::string theRaw;
};

class NameRdata: public Rdata {
	public:
		NameRdata();
		NameRdata(const std::string &name);
		virtual NameRdata *clone() const { return new NameRdata(*this); }

		virtual void clear();
		virtual void print(std::ostream & os) const;

		virtual void pack(Packer & packer) const;
		virtual void parse(Parser & parser);

	protected:
		std::string theName;
};

class CharStrRdata: public Rdata {
	public:
		CharStrRdata();
		CharStrRdata(const std::string &charStr);
		virtual CharStrRdata *clone() const { return new CharStrRdata(*this); }

		virtual void clear();
		virtual void print(std::ostream & os) const;

		virtual void pack(Packer & packer) const;
		virtual void parse(Parser & parser);

	protected:
		std::string theCharStr;
};

// ============================================================================== //

class ARdata: public RawRdata {
	public:
		ARdata();
		ARdata(const std::string &addr);
		virtual ARdata *clone() const { return new ARdata(*this); }

		virtual void print(std::ostream & os) const;
};

class AaaaRdata: public RawRdata {
	public:
		AaaaRdata();
		AaaaRdata(const std::string &addr);
		virtual AaaaRdata *clone() const { return new AaaaRdata(*this); }

		virtual void print(std::ostream & os) const;
};

class SoaRdata: public Rdata {
	public:
		SoaRdata();
		SoaRdata(const std::string &mname, const std::string &rname, uint32_t serial, uint32_t refresh, uint32_t retry, uint32_t expire, uint32_t minimum);
		virtual SoaRdata *clone() const { return new SoaRdata(*this); }

		virtual void clear();
		virtual void print(std::ostream & os) const;

		virtual void pack(Packer & packer) const;
		virtual void parse(Parser & parser);

	private:
		std::string theMname;
		std::string theRname;
		uint32_t theSerial;
		uint32_t theRefresh;
		uint32_t theRetry;
		uint32_t theExpire;
		uint32_t theMinimum;
};

class CnameRdata: public NameRdata {
	public:
		CnameRdata();
		CnameRdata(const std::string &cname);
		virtual CnameRdata *clone() const { return new CnameRdata(*this); }
};

class MxRdata: public Rdata {
	public:
		MxRdata();
		MxRdata(uint16_t pref, const std::string &exchange);
		virtual MxRdata *clone() const { return new MxRdata(*this); }

		virtual void clear();
		virtual void print(std::ostream & os) const;

		virtual void pack(Packer & packer) const;
		virtual void parse(Parser & parser);

	private:
		uint16_t thePref;
		std::string theExchange;
};

class TxtRdata: public CharStrRdata {
	public:
		TxtRdata();
		TxtRdata(const std::string &txt);
		virtual TxtRdata *clone() const { return new TxtRdata(*this); }
};

class RrSigRdata: public Rdata {
	public:
		RrSigRdata();
		RrSigRdata(uint16_t type, uint8_t algorithm, uint8_t labels,
				uint32_t ttl, uint32_t expiration, uint32_t inception, uint16_t keytag,
				const std::string & signer, const std::string & signature);

		virtual RrSigRdata *clone() const { return new RrSigRdata(*this); }

		virtual void clear();
		virtual void print(std::ostream & os) const;

		virtual void pack(Packer & packer) const;
		virtual void parse(Parser & parser);
	private:
		uint16_t theTypeCovered;//XXX: must be of RR::Type
		uint8_t theAlgorithm;
		uint8_t theLabels;
		uint32_t theTtl;
		uint32_t theExpiration;
		uint32_t theInception;
		uint16_t theKeyTag;
		std::string theSignerName;
		std::string theSignature;
};

class DnsKeyRdata: public Rdata {
	public:
		DnsKeyRdata();
		DnsKeyRdata(uint16_t flags, uint8_t protocol, uint8_t algorithm, const std::string & key);

		virtual DnsKeyRdata *clone() const { return new DnsKeyRdata(*this); }

		virtual void clear();
		virtual void print(std::ostream & os) const;

		virtual void pack(Packer & packer) const;
		virtual void parse(Parser & parser);
	private:
		uint16_t theFlags;
		uint8_t theProtocol;
		uint8_t theAlgorithm;
		std::string theKey;
};

class DsRdata: public Rdata {
	public:
		DsRdata();
		DsRdata(uint16_t key, uint8_t algorithm, uint8_t type, const std::string & digest);

		virtual DsRdata *clone() const { return new DsRdata(*this); }

		virtual void clear();
		virtual void print(std::ostream & os) const;

		virtual void pack(Packer & packer) const;
		virtual void parse(Parser & parser);
	private:
		uint16_t theKeyTag;
		uint8_t theAlgorithm;
		uint8_t theDigestType;
		// actual digest size may differ from string size; see RFC 4034
		std::string theDigest;
};

class HexBytesString {
	public:
		explicit HexBytesString(const std::string& s): bytes(s) {}
		std::ostream& print(std::ostream& os) const;
	private:
		const std::string bytes;
};

inline std::ostream &operator <<(std::ostream &os, const HexBytesString &s) { return s.print(os); }

}


#endif
