#ifndef DNSPAXXER_RR_H
#define DNSPAXXER_RR_H

#include <DnsPaxxer/common.h>

namespace DnsPaxxer {

class Packer;
class Parser;
class Rdata;

// DNS Resource Record class
class RR {
	public:
		friend class RRs;

		enum Type {
			T_UNKNOWN = 0,
			T_A = 1,
			T_NS = 2,
			T_CNAME = 5,
			T_SOA = 6,
			T_WKS = 11,
			T_PTR = 12,
			T_HINFO = 13,
			T_MX = 15,
			T_TXT = 16,
			T_SIG = 24,
			T_KEY = 25,
			T_NXT = 30,
			T_AAAA = 28,
			T_SRV = 33,
			T_CERT = 37,
			T_A6 = 38,
			T_OPT = 41,
			T_DS = 43,
			T_RRSIG = 46,
			T_NSEC = 47,
			T_DNSKEY = 48,
			T_NSEC3 = 50,
			T_AXFR = 252,
			T_IXFR = 251,
			T_ANY = 255,
			T_DLV = 32769
		};

		enum QuestClass {
			C_IN = 1,
			C_CH = 3,
			C_HS = 4,
			C_ANY = 255
		};

		class TypeString {
			public:
				explicit TypeString(const uint16_t t): theType(t) {}
				std::ostream& print(std::ostream& os) const;
			private:
				const uint16_t theType;
		};
		class ClassString {
			public:
				explicit ClassString(const uint16_t c): theClass(c) {}
				std::ostream& print(std::ostream& os) const;
			private:
				const uint16_t theClass;
		};

	public:
		RR();
		RR(std::string name, uint32_t ttl, uint16_t rclass, uint16_t type, Rdata *rdata);
		RR(const RR &r);
		~RR();
		RR &operator =(const RR &r);

		void pack(Packer & packer, bool bQuery) const;
		void parse(Parser & parser, bool bQuery);

		// [re]set the record's domain name
		void name(const std::string &aName) {
			theName = aName;
		}

		// return the record's domain name
		const std::string &name() const {
			return theName;
		}

		// just like name() except that empty string becomes "."
		const std::string &printableName() const;

		// return the record type
		uint16_t type() const {
			return theType;
		}

		// reset the record type
		void type(const uint16_t aType) {
			theType = aType;
		}

		TypeString strType() const {
			return TypeString(theType);
		}

		// return the record class
		uint16_t rclass() const {
			return theClass;
		}

		// reset the record class
		void rclass(const uint16_t c) {
			theClass = c;
		}

		ClassString strClass() const {
			return ClassString(theClass);
		}

		// return the record time-to-live (TTL)
		uint32_t ttl() const {
			return theTTL;
		}

		// reset the record time-to-live (TTL)
		void ttl(const uint32_t t) {
			theTTL = t;
		}

		// returns record's data, possibly nil
		Rdata *rdata() const {
			return theRdata;
		}

		// resets record's data, possibly to nil; absorbs the passed value
		void absorbRdata(Rdata *rdata);

		// return a string address if the record is of type A or AAAA
		std::string address() const;

		// dump RR members
		std::ostream& print(std::ostream& os) const;

	private:
		std::string theName;
		uint16_t theType;
		uint16_t theClass;
		uint32_t theTTL;
		Rdata *theRdata;
};

inline std::ostream &operator <<(std::ostream &os, const RR &rr) { return rr.print(os); }
inline std::ostream &operator <<(std::ostream &os, const RR::TypeString &t) { return t.print(os); }
inline std::ostream &operator <<(std::ostream &os, const RR::ClassString &c) { return c.print(os); }

}

#endif
