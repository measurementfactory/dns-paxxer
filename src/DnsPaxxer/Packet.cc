#include <DnsPaxxer/Packet.h>
#include <DnsPaxxer/Packer.h>
#include <DnsPaxxer/Parser.h>
#include <DnsPaxxer/RRs.h>
#include <xstd/TextException.h>
#include <xstd/Assert.h>
#include <memory>

char const * DnsPaxxer::Packet::sNames[] = { "Header", "Question", "Answer", "Authority", "Additional" };

DnsPaxxer::Packet::Packet(): needsSync(false) {
}

DnsPaxxer::Packet::Packet(const char *data, Packet::size_type size): needsSync(false) {
	Parser parser(data, size);
	parse(parser);
}

void DnsPaxxer::Packet::clear() {
	// set needsSync to true before clearing
	// in case .clear() throws even if it currently does not
	needsSync = true;

	header().clear();
	questions().clear();
	answers().clear();
	authorities().clear();
	additions().clear();

	// set needsSync to false because at that point we are again in-sync
	needsSync = false;
}

void DnsPaxxer::Packet::parse(Parser & parser, Section upto) {
	clear(); // clear all in case we throw before parsing some part

	if (parser.bufferSize() < 12)
	    throw Texc("message too short");

	// parseSection and others below may throw, leaving us out of sync
	needsSync = true;

	header().parse(parser);

	if (upto >= S_QUESTION)
		questions().parse(parser, header().qdcount(), true);

	if (upto >= S_ANSWER)
		answers().parse(parser, header().ancount(), false);

	if (upto >= S_AUTHORITY)
		authorities().parse(parser, header().nscount(), false);

	if (upto >= S_ADDITIONAL)
		additions().parse(parser, header().arcount(), false);

	sync();
}

void DnsPaxxer::Packet::pack(Packer & packer) const {
	if (needsSync)
		throw Texc("Packet may be out-of-sync.  Use sync() method");

	header().pack(packer);

	questions().pack(packer, true);
	answers().pack(packer, false);
	authorities().pack(packer, false);
	additions().pack(packer, false);
}

DnsPaxxer::Header& DnsPaxxer::Packet::header() {
	return theHeader;
}

const DnsPaxxer::Header& DnsPaxxer::Packet::header() const {
	return theHeader;
}

bool DnsPaxxer::Packet::findEdns(EDNS &edns) const {
	const DnsPaxxer::RRs &extras = additions();

	for (RRs::const_iterator i = extras.begin(); i != extras.end(); ++i) {
		if ((*i)->type() == RR::T_OPT) {
			edns.load(**i);
			return true;
		}
	}

	return false; // not found
}

void DnsPaxxer::Packet::putEdns(const EDNS &edns) {
	DnsPaxxer::RRs &extras = additions();

	// try to update an existing record first
	for (RRs::iterator i = extras.begin(); i != extras.end(); ++i) {
		if ((*i)->type() == RR::T_OPT) {
			edns.store(**i);
			return;
		}
	}

	// no RR::T_OPT records found; must add one
	RR *rr = new RR;
	edns.store(*rr);
	extras.push_back(rr);
}

bool DnsPaxxer::Packet::deleteEdns() {
	const bool neededSync = needsSync;
	DnsPaxxer::RRs &extras = additions();
	bool deleted = false;

	// try to update an existing record first
	for (RRs::iterator i = extras.begin(); i != extras.end();) {
		if ((*i)->type() == RR::T_OPT) {
			i = extras.erase(i);
			deleted = true;
		} else {
			++i;
		}
	}

	needsSync = neededSync || deleted;
	return deleted;
}

DnsPaxxer::RRs& DnsPaxxer::Packet::questions() {
	needsSync = true;
	return theQuestions;
}

const DnsPaxxer::RRs& DnsPaxxer::Packet::questions() const {
	return theQuestions;
}

DnsPaxxer::RRs& DnsPaxxer::Packet::answers() {
	needsSync = true;
	return theAnswers;
}

const DnsPaxxer::RRs& DnsPaxxer::Packet::answers() const {
	return theAnswers;
}

DnsPaxxer::RRs& DnsPaxxer::Packet::authorities() {
	needsSync = true;
	return theAuthorities;
}

const DnsPaxxer::RRs& DnsPaxxer::Packet::authorities() const {
	return theAuthorities;
}

DnsPaxxer::RRs& DnsPaxxer::Packet::additions() {
	needsSync = true;
	return theAdditions;
}

const DnsPaxxer::RRs& DnsPaxxer::Packet::additions() const {
	return theAdditions;
}

void DnsPaxxer::Packet::sync() {
	if (!needsSync)
		return;
	header().theQdCount = (uint16_t)questions().size();
	header().theAnCount = (uint16_t)answers().size();
	header().theNsCount = (uint16_t)authorities().size();
	header().theArCount = (uint16_t)additions().size();
	needsSync = false;
}

std::ostream& DnsPaxxer::Packet::print(std::ostream& os) const {
	if (needsSync)
		os << ";; WARNING: Packet may be out-of-sync" << endl;
	header().print(os);

	// print EDNS, if any. Skip additional section that contains EDNS only
	RRs::size_type printableExtrasSize = 1;
	EDNS edns;
	if (findEdns(edns)) {
		edns.print(os);
		++printableExtrasSize;
	}

	if(!questions().empty())
		questions().print(os<<"\n;; QUESTION SECTION:\n");
	if(!answers().empty())
		answers().print(os<<"\n;; ANSWER SECTION:\n");
	if(!authorities().empty())
		authorities().print(os<<"\n;; AUTHORITY SECTION:\n");

	if (additions().size() >= printableExtrasSize)
		additions().print(os<<"\n;; ADDITIONAL SECTION:\n");

	return os;
}

const DnsPaxxer::RR& DnsPaxxer::Packet::firstQuestion() const {
	if (questions().empty())
	    throw Texc("No question in Packet");
	return *(questions().front());
}
