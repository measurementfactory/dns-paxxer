#ifndef DNSPAXXER_FORWARD_H
#define DNSPAXXER_FORWARD_H

// forward declarations of commonly used interface classes
namespace DnsPaxxer {
	class Packet;
	class Header;
	class RR;
	class RRs;
}

#endif /* DNSPAXXER_FORWARD_H */

