#include <DnsPaxxer/Packer.h>
#include <xstd/TextException.h>
#include <xstd/Assert.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/nameser.h>
#include <resolv.h>
#include <memory>
#include <cstring>

DnsPaxxer::Packer::Packer(char *buf, const size_type bufSize): theBuf(buf),
	theBufSize(bufSize), theBufOffset(0) {

	theNames[0] = (unsigned char*)buf; // should point to packet buffer
	theNames[1] = 0; // the end of known names (we do not know any yet)
}

DnsPaxxer::Packer::~Packer() {
}

void DnsPaxxer::Packer::pack(const uint8_t val) {
	if (theBufOffset + sizeof(uint8_t) > theBufSize)
		throw Texc("no space to pack a 8bit int");
	theBuf[theBufOffset++] = val;
}

void DnsPaxxer::Packer::pack(const uint16_t val) {
	const uint16_t src = htons(val);
	if (theBufOffset + sizeof(src) > theBufSize)
		throw Texc("no space to pack a 16bit int");
	memcpy(theBuf + theBufOffset, &src, sizeof(src));
	theBufOffset += sizeof(src);
}

void DnsPaxxer::Packer::pack(const uint32_t val) {
	const uint32_t src = htonl(val);
	if (theBufOffset + sizeof(src) > theBufSize)
		throw Texc("no space to pack a 32bit int");
	memcpy(theBuf + theBufOffset, &src, sizeof(src));
	theBufOffset += sizeof(src);
}

DnsPaxxer::Packer::size_type DnsPaxxer::Packer::skipLengthField() {
	const size_type pos = theBufOffset;
	const size_type LengthFieldSize = 2;
	if (theBufOffset + LengthFieldSize > theBufSize)
		throw Texc("no space for the length field");
	theBufOffset += LengthFieldSize;
	return pos;
}

void DnsPaxxer::Packer::bakeLengthField(const size_type pos) {
	if(theBufOffset < (pos + 2))
		throw Texc("invalid offset");

	const uint16_t val = htons(theBufOffset - pos - 2);
	Assert(val > 0); // we put something after we skipped; TODO: remove?
	memcpy(theBuf + pos, &val, sizeof(val));
	// theBufOffset is unchanged
}

void DnsPaxxer::Packer::packName(const std::string & name, bool allowCompression) {
	// we cannot guess the packed length name; hopefully dn_comp is careful
	if (theBufSize < theBufOffset + name.length())
		throw Texc("name may not fit");

	const int compressedLen = dn_comp(name.c_str(),
		(unsigned char *)(theBuf+theBufOffset),
		theBufSize - theBufOffset,
		allowCompression ? theNames : 0,
		allowCompression ? (theNames + NameLimit) : 0);

	if (compressedLen < 0)
		throw Texc("dn_comp(3) failed");

	theBufOffset += compressedLen;

	// not an exception because dn_comp already corrupted our memory
	Assert(theBufOffset <= theBufSize);
}

void DnsPaxxer::Packer::packCharString(const std::string & name) {
	const unsigned char size = static_cast<unsigned char>(name.size());

	if (theBufOffset + size >= theBufSize) // '=' accounts for 1-byte size
		throw Texc("CharString does not fit");

	theBuf[theBufOffset++] = size;
	memcpy(theBuf + theBufOffset, name.data(), size);
	theBufOffset += size;
}

void DnsPaxxer::Packer::packRawData(const std::string & rawData, const uint16_t size) {
	if (theBufOffset + size > theBufSize)
		throw Texc("RawData does not fit");

	memcpy(theBuf + theBufOffset, rawData.data(), size);
	theBufOffset += size;
}

void DnsPaxxer::SetQueryId(char *const buf, const size_t size, const uint16_t queryId) {
	DnsPaxxer::Packer packer(buf, size);
	packer.pack(queryId);
}
