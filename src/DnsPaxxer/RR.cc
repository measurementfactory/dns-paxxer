#include <DnsPaxxer/Packet.h>
#include <DnsPaxxer/Packer.h>
#include <DnsPaxxer/Parser.h>
#include <DnsPaxxer/Rdata.h>
#include <DnsPaxxer/RR.h>
#include <xstd/TextException.h>
#include <xstd/BinStream.h>
#include <xstd/Assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <arpa/inet.h>

typedef std::pair<int, std::string> RclassTextEntry;
static const RclassTextEntry rclassrecords[4] = {
	RclassTextEntry(DnsPaxxer::RR::C_IN, "IN"),
	RclassTextEntry(DnsPaxxer::RR::C_CH, "CHAOS"),
	RclassTextEntry(DnsPaxxer::RR::C_HS, "HESIOD"),
	RclassTextEntry(DnsPaxxer::RR::C_ANY, "ANY")
};
typedef std::map<int, std::string> RclassText;
//XXX: remove explicit array size
static const RclassText TheRclassText(rclassrecords, rclassrecords+4);

typedef std::pair<int, std::string> TypeTextEntry;
static const TypeTextEntry typerecords[25] = {
	TypeTextEntry(DnsPaxxer::RR::T_UNKNOWN, "UNKNOWN"),
	TypeTextEntry(DnsPaxxer::RR::T_A, "A"),
	TypeTextEntry(DnsPaxxer::RR::T_NS, "NS"),
	TypeTextEntry(DnsPaxxer::RR::T_CNAME, "CNAME"),
	TypeTextEntry(DnsPaxxer::RR::T_SOA, "SOA"),
	TypeTextEntry(DnsPaxxer::RR::T_WKS, "WKS"),
	TypeTextEntry(DnsPaxxer::RR::T_PTR, "PTR"),
	TypeTextEntry(DnsPaxxer::RR::T_HINFO, "HINFO"),
	TypeTextEntry(DnsPaxxer::RR::T_MX, "MX"),
	TypeTextEntry(DnsPaxxer::RR::T_TXT, "TXT"),
	TypeTextEntry(DnsPaxxer::RR::T_SIG, "SIG"),
	TypeTextEntry(DnsPaxxer::RR::T_KEY, "KEY"),
	TypeTextEntry(DnsPaxxer::RR::T_NXT, "NXT"),
	TypeTextEntry(DnsPaxxer::RR::T_AAAA, "AAAA"),
	TypeTextEntry(DnsPaxxer::RR::T_SRV, "SRV"),
	TypeTextEntry(DnsPaxxer::RR::T_CERT, "CERT"),
	TypeTextEntry(DnsPaxxer::RR::T_A6, "A6"),
	TypeTextEntry(DnsPaxxer::RR::T_OPT, "OPT"),
	TypeTextEntry(DnsPaxxer::RR::T_DS, "DS"),
	TypeTextEntry(DnsPaxxer::RR::T_RRSIG, "RRSIG"),
	TypeTextEntry(DnsPaxxer::RR::T_NSEC, "NSEC"),
	TypeTextEntry(DnsPaxxer::RR::T_DNSKEY, "DNSKEY"),
	TypeTextEntry(DnsPaxxer::RR::T_AXFR, "AXFR"),
	TypeTextEntry(DnsPaxxer::RR::T_IXFR, "IXFR"),
	TypeTextEntry(DnsPaxxer::RR::T_ANY, "ANY")
};
typedef std::map<int, string> TypeText;
//XXX: remove explicit array size
static TypeText TheTypeText(typerecords, typerecords+24);

std::ostream& DnsPaxxer::RR::TypeString::print(std::ostream& os) const {
	static const std::string type("TYPE");

	// XXX: optimize; map search is too slow for this
	const TypeText::const_iterator i = TheTypeText.find(theType);
	if (i == TheTypeText.end())
		os << type << theType;
	else
		os << i->second;
	return os;
}

std::ostream& DnsPaxxer::RR::ClassString::print(std::ostream& os) const {
	static const std::string rclass("CLASS"); // presumed thread safe

	// XXX: optimize; map search is too slow for this
	const RclassText::const_iterator i = TheRclassText.find(theClass);
	if (i == TheRclassText.end())
		os << rclass << theClass;
	else
		os << i->second;
	return os;
}


DnsPaxxer::RR::RR(): theType(T_UNKNOWN), theClass(C_ANY), theTTL(0), theRdata(0) {
}

DnsPaxxer::RR::RR(std::string name, uint32_t ttl, uint16_t rclass, uint16_t type, Rdata *rdata):
	theName(name), theType(type), theClass(rclass), theTTL(ttl), theRdata(rdata) {
}

DnsPaxxer::RR::RR(const RR &r):
	theName(r.name()), theType(r.type()), theClass(r.rclass()), theTTL(r.ttl()), 
	theRdata(r.rdata() ? r.rdata()->clone() : 0) {
}

DnsPaxxer::RR::~RR() {
	delete theRdata;
}

DnsPaxxer::RR& DnsPaxxer::RR::operator =(const RR &r) {
	if (this != &r) {
		theName = r.name();
		theType = r.type();
		theClass = r.rclass();
		theTTL = r.ttl();
		absorbRdata(r.rdata() ? r.rdata()->clone() : 0);
	}
	return *this;
}

void DnsPaxxer::RR::absorbRdata(Rdata *d) {
	Must(!theRdata || theRdata != d); 
	delete theRdata;
	theRdata = d; // maybe nil
}

const std::string& DnsPaxxer::RR::printableName() const {
	static const std::string dot = ".";
	return theName.length() ? theName : dot;
}

std::string DnsPaxxer::RR::address() const {
	// convert opaque data() string into ip string adress
	switch(type()) {
	case T_A:
	case T_AAAA:
		Must(theRdata);
		return theRdata->string();
		break;
	default:
		throw Texc("error: address() method is bad for non-A and non-AAAA records");
		break;
	}
}

std::ostream& DnsPaxxer::RR::print(std::ostream& os) const {
	// TODO: Packet::print() prints EDNS, but this check feels wrong;
	// Packet should probably skip OPT if it does not want them printed
	if (T_OPT == type()) 
		return os;
	os	<< std::setw(25) << std::left << name() + '.'
		<< std::setw(8) << std::right << ttl() << "  "
		<< std::setw(8) << std::left << strClass()
		<< std::setw(8) << std::left << strType()
		<< (theRdata ? theRdata->string() : "")
		<< std::endl;

	return os;
}

void DnsPaxxer::RR::pack(Packer & packer, bool bQuery) const {
	packer.packName(theName);

	packer.pack(theType);
	packer.pack(theClass);
	
	// that's enough for qrecord
	if(bQuery)
		return;

	packer.pack(theTTL);

	// it is possible to have an RR without rdata, but RDLENGTH is required
	if (theRdata)
		theRdata->pack(packer);
	else
		packer.pack(static_cast<uint16_t>(0));
}

void DnsPaxxer::RR::parse(Parser & parser, bool bQuery) {
	parser.parseName(theName);

	theType = parser.parse16bit();
	theClass = parser.parse16bit();

	if(bQuery)
		return;

	parser.parse(theTTL);

	Must(!theRdata);
	switch(theType) {
	case DnsPaxxer::RR::T_MX:
		theRdata = new MxRdata;
		break;
	case DnsPaxxer::RR::T_NS:
		theRdata = new NameRdata;
		break;
	case DnsPaxxer::RR::T_CNAME:
		theRdata = new CnameRdata;
		break;
	case DnsPaxxer::RR::T_TXT:
		theRdata = new CharStrRdata;
		break;
	case DnsPaxxer::RR::T_SOA:
		theRdata = new SoaRdata;
		break;
	case DnsPaxxer::RR::T_A:
		theRdata = new ARdata;
		break;
	case DnsPaxxer::RR::T_AAAA:
		theRdata = new AaaaRdata;
		break;
	case DnsPaxxer::RR::T_DNSKEY:
		theRdata = new DnsKeyRdata;
		break;
	case DnsPaxxer::RR::T_RRSIG:
		theRdata = new RrSigRdata;
		break;
	case DnsPaxxer::RR::T_DS:
		theRdata = new DsRdata;
		break;
/*
	case DnsPaxxer::RR::T_OPT:
	case DnsPaxxer::RR::T_NSEC:
*/
	default:
		theRdata = new RawRdata;
		break;
	}

	Must(theRdata);
	theRdata->parse(parser);
}
