#ifndef DNSPAXXER_PACKET_H
#define DNSPAXXER_PACKET_H

#include <DnsPaxxer/common.h>
#include <DnsPaxxer/RR.h>
#include <DnsPaxxer/RRs.h>
#include <DnsPaxxer/Header.h>
#include <DnsPaxxer/EDNS.h>

namespace DnsPaxxer {

class Packer;
class Parser;

// DNS packet object class
class Packet {
	public:
		typedef std::string::size_type size_type;

                enum Section {
			S_HEADER,
                        S_QUESTION,
                        S_ANSWER,
                        S_AUTHORITY,
                        S_ADDITIONAL
                };


	public:
		Packet();
		Packet(const char *data, size_type size); // calls raw()

		void clear(); // optimized reinitialization, may not deallocate RAM

		// parses the packet; throws on failures
		void parse(Parser & parser, Section upto = S_ADDITIONAL);

		// forms the packet; throws on failures
		void pack(Packer & packer) const;

		Header &header();
		const Header &header() const;

		bool findEdns(EDNS &edns) const;
		void putEdns(const EDNS &edns);
		bool deleteEdns(); // deletes all OPT RRs; returns true if deleted any

		RRs &questions();
		const RRs &questions() const;

		RRs &answers();
		const RRs &answers() const;

		RRs &authorities();
		const RRs &authorities() const;

		RRs &additions();
		const RRs &additions() const;

		// syncs Header with RRs
		void sync();

		std::ostream& print(std::ostream& os) const;

		const RR& firstQuestion() const; // convenience

	private:
		Header theHeader;
		RRs theQuestions, theAnswers, theAuthorities, theAdditions;
		bool needsSync;

		static const char *sNames[];
};

inline std::ostream &operator <<(std::ostream &os, const Packet &pkt) { return pkt.print(os); }

}

#endif
