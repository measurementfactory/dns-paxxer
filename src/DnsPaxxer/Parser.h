#ifndef DNSPAXXER_PARSER_H
#define DNSPAXXER_PARSER_H

#include <DnsPaxxer/common.h>

namespace DnsPaxxer {

// Parser class that is responsible
// for mantaining parsing state information
class Parser {
	public:
		typedef std::string::size_type size_type;

		Parser(const char *buf, const size_type size);
		~Parser();

		size_type bufferSize() const { return theBufSize; }
		size_type offset() const { return theBufOffset; }

		void parseName(std::string &name);
		void parseCharString(std::string &str);
		void parseRawData(std::string &data, const uint16_t size);

		uint8_t parse8bit();
		void parse(uint8_t & val);

		uint16_t parseRdLength();
		uint16_t parse16bit();
		void parse(uint16_t & val);

		uint32_t parse32bit();
		void parse(uint32_t & val);
		//XXX: add more methods

	private:
		const char *theBuf;
		size_type theBufSize;
		size_type theBufOffset;

	private:
		Parser(const Parser & src);
		Parser & operator = (const Parser & src);
};

}
#endif
