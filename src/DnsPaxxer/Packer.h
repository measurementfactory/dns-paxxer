#ifndef DNSPAXXER_PACKER_H
#define DNSPAXXER_PACKER_H

#include <DnsPaxxer/common.h>

namespace DnsPaxxer {

// Packer class that is responsible
// for mantaining packing state information
class Packer {
	public:
		typedef std::string::size_type size_type;

		Packer(char *buf, const size_type bufSize);
		~Packer();

		// size of packed data in buffer
		size_type offset() const { return theBufOffset; }

		void packCharString(const std::string & name);
		void packRawData(const std::string & rawData, const uint16_t size);
		void packName(const std::string & name, bool allowCompression=true);
		void pack(const uint8_t val);
		void pack(const uint16_t val);
		void pack(const uint32_t val);

		size_type skipLengthField();
		void bakeLengthField(const size_type pos);
		//XXX: add more methods

	private:
		char * theBuf;
		size_type theBufSize;
		size_type theBufOffset;

		static const int NameLimit = 50;
		unsigned char *theNames[NameLimit];

	private:
		Packer(const Packer & src);
		Packer & operator = (const Packer & src);
};

// sets query ID, assuming the buffer contains a packed DNS message;
// throws if the buffer is too short to hold a query ID
void SetQueryId(char *const buf, const size_t size, const uint16_t queryId);

}
#endif
