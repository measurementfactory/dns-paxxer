#ifndef DNSPAXXER_RRS_H
#define DNSPAXXER_RRS_H

#include <DnsPaxxer/common.h>

namespace DnsPaxxer {

class RR;
class Packer;
class Parser;

// Array of DNS Resource Records
typedef std::vector<DnsPaxxer::RR*> BaseRRs;

class RRs: public DnsPaxxer::BaseRRs {
	public:
		RRs();
		RRs(const RRs & rrs);
		RRs & operator = (const RRs & rrs);

		~RRs() { clear(); }

		void pack(Packer & packer, bool bQuery) const;
		void parse(Parser & parser, uint16_t sCount, bool bQuery);

		void clear();
		std::ostream& print(std::ostream& os) const;
};

inline std::ostream &operator <<(std::ostream &os, const RRs &rrs) { return rrs.print(os); }

}

#endif
