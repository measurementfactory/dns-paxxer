#ifndef DNSPAXXER_COMMON_H
#define DNSPAXXER_COMMON_H

#if defined(HAVE_CONFIG_H)
#       include <DnsPaxxer/config.h>
#endif

#include <string>
#include <iostream>
#include <iosfwd>
#include <sstream>
#include <vector>
#include <map>
#include <iomanip>

#include <stdint.h>

#endif
