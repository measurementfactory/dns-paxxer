#include <DnsPaxxer/Header.h>
#include <DnsPaxxer/Packer.h>
#include <DnsPaxxer/Parser.h>

static const char *opcodetext[] = {
	"QUERY",
	"IQUERY",
	"STATUS",
	"RESERVED3",
	"NOTIFY",
	"UPDATE",
	"RESERVED6",
	"RESERVED7",
	"RESERVED8",
	"RESERVED9",
	"RESERVED10",
	"RESERVED11",
	"RESERVED12",
	"RESERVED13",
	"RESERVED14",
	"RESERVED15"
};

static const char *rcodetext[] = {
	"NOERROR",
	"FORMERR",
	"SERVFAIL",
	"NXDOMAIN",
	"NOTIMP",
	"REFUSED",
	"YXDOMAIN",
	"YXRRSET",
	"NXRRSET",
	"NOTAUTH",
	"NOTZONE",
	"RESERVED11",
	"RESERVED12",
	"RESERVED13",
	"RESERVED14",
	"RESERVED15",
	"BADVERS"
};

DnsPaxxer::Header::Header():
		theOpCode(opcodeStdQuery), theRcode(rcodeNoError),
		theId((uint16_t)-1), //XXX: decide correct values
		theQdCount(0), theAnCount(0), theNsCount(0), theArCount(0),
		theQr(false), theAa(false), theTc(false), theRd(false), theCd(false),
		theRa(false), theAd(false) {
}

void DnsPaxxer::Header::clear() {
	*this = DnsPaxxer::Header();
}

void DnsPaxxer::Header::pack(Packer & packer) const {
	uint16_t flags = opcode() << opcodeShift;

	if(qr()) flags |= DnsPaxxer::Header::qrMask; // for responses
	if(aa()) flags |= DnsPaxxer::Header::flagAA;
	if(tc()) flags |= DnsPaxxer::Header::flagTC;
	if(rd()) flags |= DnsPaxxer::Header::flagRD;
	if(ra()) flags |= DnsPaxxer::Header::flagRA;
	if(ad()) flags |= DnsPaxxer::Header::flagAD;
	flags |= rcode();

	packer.pack(id());
	packer.pack(flags);

	packer.pack(theQdCount);
	packer.pack(theAnCount);
	packer.pack(theNsCount);
	packer.pack(theArCount);
}

void DnsPaxxer::Header::parse(Parser & parser) {
	uint16_t tmp;

	parser.parse(tmp);
	id(tmp);

	parser.parse(tmp);
	const uint16_t flags = tmp;

	qr( (bool)(flags & DnsPaxxer::Header::qrMask) );
	opcode( (DnsPaxxer::Header::OpCodes)((flags & opcodeMask) >> opcodeShift) );
	rcode( (DnsPaxxer::Header::RCodes)(flags & DnsPaxxer::Header::rcodeMask) );
	aa( (bool)(flags & DnsPaxxer::Header::flagAA) );
	tc( (bool)(flags & DnsPaxxer::Header::flagTC) );
	rd( (bool)(flags & DnsPaxxer::Header::flagRD) );
	ra( (bool)(flags & DnsPaxxer::Header::flagRA) );
	ad( (bool)(flags & DnsPaxxer::Header::flagAD) );
	cd( (bool)(flags & DnsPaxxer::Header::flagCD) );
	//XXX: check Z-flag for zero value

	parser.parse(theQdCount);
	parser.parse(theAnCount);
	parser.parse(theNsCount);
	parser.parse(theArCount);
	//XXX: any other checking?
}

std::ostream& DnsPaxxer::Header::print(std::ostream& os) const {
	os  << ";; ->>HEADER<<- "
		<< "opcode: " << opcodetext[opcode()] << ", "
		<< "status: " << rcodetext[rcode()] << ", "
		<< "id: " << id()
		<< std::endl;

	os  << ";; flags:"
			<< (qr()?" qr":"") << (aa()?" aa":"")
			<< (tc()?" tc":"") << (rd()?" rd":"")
			<< (cd()?" cd":"") << (ra()?" ra":"")
			<< (ad()?" ad":"")
		<< "; "
		<< "QUERY: "     << qdcount() << ", "
		<< "ANSWER: "    << ancount() << ", "
		<< "AUTHORITY: " << nscount() << ", "
		<< "ADDITIONAL: "<< arcount() << std::endl;

	return os;
}
