#include <DnsPaxxer/EDNS.h>
#include <DnsPaxxer/RR.h>
#include <DnsPaxxer/Packer.h>
#include <xstd/Assert.h>
#include <netinet/in.h>

DnsPaxxer::EDNS::EDNS():
		theVersion(0),
		theUdpSize(0),
		theDo(false),
		hasInfo(false) {
}

std::ostream& DnsPaxxer::EDNS::print(std::ostream& os) const {
	if (!has_info())
		return os;
	os << ";; OPT PSEUDOSECTION: " << std::endl;
	os << "; EDNS: version: " << (int) version() << ", "
		<< "flags:"
			<< (dnssec_okay()?" do":"")
		<< "; "
		<< "udp: " << udpSize() << std::endl;
	os << std::endl;
	return os;
}

/*
 *              +0 (MSB)                +1 (LSB)
 *       +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *    0: |   EXTENDED-RCODE      |       VERSION         |
 *       +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *    2: |DO|                    Z                       |
 *       +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
*/

void DnsPaxxer::EDNS::load(const RR &rr) {
	hasInfo = true;

	Must(rr.type() == DnsPaxxer::RR::T_OPT);

	udpSize(rr.rclass());

	const uint32_t fakeTtl = rr.ttl();
	version((fakeTtl>>16) & 0xFF);

	const uint16_t flags = fakeTtl & 0xFFFF;
	dnssec_okay(flags & EDNS::flagDO);
}

void DnsPaxxer::EDNS::store(RR &rr) const {
	// store even if !hasInfo because 
	if (!hasInfo)
		return;

	uint16_t flags = 0;
	if (theDo)
		flags |= EDNS::flagDO;

	rr.name(std::string());
	rr.type(DnsPaxxer::RR::T_OPT);
	rr.rclass(theUdpSize);
	rr.ttl(static_cast<uint32_t>(flags | (theVersion << 16)));
	rr.absorbRdata(0);
}
