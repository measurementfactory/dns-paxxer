#ifndef DNSPAXXER_DNSPAXXER_H
#define DNSPAXXER_DNSPAXXER_H

// Include all commonly used headers for user convenience
#include "DnsPaxxer/Packet.h"
#include "DnsPaxxer/Header.h"
#include "DnsPaxxer/RR.h"
#include "DnsPaxxer/RRs.h"
#include "DnsPaxxer/Packer.h"
#include "DnsPaxxer/Parser.h"

#endif
