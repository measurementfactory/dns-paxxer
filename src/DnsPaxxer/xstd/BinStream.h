/* (C) 2005  The Measurement Factory
   Licensed under the Apache License, Version 2.0 */

#ifndef TMF_BASE_XSTD__BINSTREAM_H
#define TMF_BASE_XSTD__BINSTREAM_H

///#include <xstd/iosfwd.h>
///#include <xstd/Time.h>
///#include <xstd/Size.h>
///#include <xstd/NetAddr.h>
///#include <xstd/NetInet.h>    /* for ntoh and hton */

// a collection of streams that store binary objects, without
// conversion to their textual representation common to STL iostreams;
// storage format is platform-independent (network byte order)

///struct sockaddr_storage;

///->
#include <cstring>
#include <iostream>
#include <netinet/in.h>

#define SizeOf(v) sizeof(v)
typedef int Size;
using namespace std;
///<-

class IBinStream {
	public:
		typedef streamoff off_type;
		typedef void (Manip)(class IBinStream &);

	public:
		IBinStream();

		void open(istream *aStream);

		bool good() const { return theStream && theStream->good(); }

		char getChar() { char c; get(&c, 1); return c; }
		bool getb() { return getChar() != 0; }

		int geti() { int x; return geti(x); }
		int geti(int &x) { get(&x, SizeOf(x)); return x = ntohl(x); }

		unsigned short getUsi() { unsigned short x; return getUsi(x); }
		unsigned short getUsi(unsigned short &x) { get(&x, SizeOf(x)); return x = ntohs(x); }

		unsigned long getUi() { uint32_t x; return getUi(x); }
		unsigned long getUi(uint32_t &x) { get(&x, SizeOf(x)); return x = ntohl(x); }

///		struct sockaddr_storage &geta(struct sockaddr_storage &a);
		string &gets(string &s);

		// use to get raw strings only
		void get(void *buf, Size size);

		void skip(Size size);

		istream *stream() { return theStream; }
		off_type offset() const { return theStream->tellg(); }

	protected:
		istream *theStream;
};

class OBinStream {
	public:
		typedef void (Manip)(class OBinStream &);

	public:
		OBinStream();
		~OBinStream();

		void open(ostream *aStream);

		bool good() const { return theStream && theStream->good(); }

		void putChar(char c) { put(&c, 1); }
		void putb(bool b) { putChar((char)(b ? 1 : 0)); }
		void puti(int x) { x = htonl(x); put(&x, SizeOf(x)); }
		void putUi(uint32_t x) { x = htonl(x); put(&x, SizeOf(x)); }
		void putUsi(unsigned short x) { x = htons(x); put(&x, SizeOf(x)); }
		void puts(const string &s) { puti(s.size()); if (s.size()) put(s.data(), s.size()); }
		void puta(const struct sockaddr_storage &a);

		// use to store raw strings only
		void put(const void *buf, Size size);

		ostream *stream() { return theStream; }
		streamoff offset() const { return theStream->tellp(); }

    protected:
        ostream *theStream;
};

class BinStream: public IBinStream, public OBinStream {
	public:
		void open(iostream *aStream);
};



/* operators for common types */

inline
IBinStream &operator >>(IBinStream &is, char &c) {
	c = is.getChar();
	return is;
}

inline
IBinStream &operator >>(IBinStream &is, bool &b) {
	b = is.getb();
	return is;
}

inline
IBinStream &operator >>(IBinStream &is, int &x) {
	is.geti(x);
	return is;
}

inline
IBinStream &operator >>(IBinStream &is, unsigned short &x) {
	is.getUsi(x);
	return is;
}

inline
IBinStream &operator >>(IBinStream &is, string &s) {
	is.gets(s);
	return is;
}

///inline
///IBinStream &operator >>(IBinStream &is, Time &t) {
///	t.tv_sec = is.geti();
///	t.tv_usec = is.geti();
///	return is;
///}

///inline
///IBinStream &operator >>(IBinStream &is, Size &sz) {
///	int s;
///	if ((is >> s).good())
///		sz = s;
///	return is;
///}

///extern IBinStream &operator >>(IBinStream &is, NetAddr &a);

inline
IBinStream &operator >>(IBinStream &is, IBinStream::Manip m) {
	m(is);
	return is; 
}

inline
OBinStream &operator <<(OBinStream &os, char c) {
	os.putChar(c);
	return os;
}

inline
OBinStream &operator <<(OBinStream &os, bool b) {
	os.putb(b);
	return os;
}

inline
OBinStream &operator <<(OBinStream &os, int x) {
	os.puti(x);
	return os;
}

inline
OBinStream &operator <<(OBinStream &os, uint32_t x) {
	os.putUi(x);
	return os;
}

inline
OBinStream &operator <<(OBinStream &os, unsigned short x) {
	os.putUsi(x);
	return os;
}

inline
OBinStream &operator <<(OBinStream &os, const char *s) {
	const int len = strlen(s);
	os.puti(len);
	if (len)
		os.put(s, len);
	return os;
}

inline
OBinStream &operator <<(OBinStream &os, const string &s) {
	os.puts(s);
	return os;
}

///inline
///OBinStream &operator <<(OBinStream &os, const Time &t) {
///	os.puti(t.tv_sec);
///	os.puti(t.tv_usec);
///	return os;
///}

///extern OBinStream &operator <<(OBinStream &os, const NetAddr &a);

inline
OBinStream &operator <<(OBinStream &os, OBinStream::Manip m) {
	m(os);
	return os;
}

#endif
