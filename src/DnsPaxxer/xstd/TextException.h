/* (C) 2005  The Measurement Factory
   Licensed under the Apache License, Version 2.0 */

#ifndef TMF_BASE_XSTD__ERROREXCEPTION_H
#define TMF_BASE_XSTD__ERROREXCEPTION_H

//#include <xstd/iostream.h>
//#include <xstd/string.h>

///->
#include <iostream>
#include <string>
using namespace std;
///<-

// simple exception to report custom errors
// we may want to change the interface to be able to report system errors
class TextException {
	public:
		TextException(const string &aMessage, const char *aFileName = 0, int aLineNo = -1);
		virtual ~TextException();

		virtual ostream &print(ostream &os) const;

	public:
		string message;

	protected:
		// optional location information
		const char *theFileName;
		int theLineNo;
};

inline
ostream &operator <<(ostream &os, const TextException &exx) {
	return exx.print(os);
}

#if !defined(Texc)
#	define Texc(msg) TextException((msg), __FILE__, __LINE__)
#endif

#endif
