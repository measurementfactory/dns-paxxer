/* (C) 2005  The Measurement Factory
   Licensed under the Apache License, Version 2.0 */

#include <xstd/config.h>

#include <iostream>

#include <xstd/TextException.h>

TextException::TextException(const string &aMsg, const char *aFileName, int aLineNo):
	message(aMsg), theFileName(aFileName), theLineNo(aLineNo) {
}

TextException::~TextException() {
}

ostream &TextException::print(ostream &os) const {
	if (theFileName)
		os << theFileName << ':' << theLineNo << ": ";
	return os << message;
}
