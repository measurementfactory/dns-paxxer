/* (C) 2005  The Measurement Factory
   Licensed under the Apache License, Version 2.0 */

#include <xstd/config.h>
#include <xstd/Assert.h>
///#include <xstd/iostream.h>
#include <xstd/BinStream.h>

#include <sys/types.h>
#include <sys/socket.h>


/* IBinStream */

IBinStream::IBinStream(): theStream(0) {
}

void IBinStream::open(istream *aStream) {
	Assert(!theStream && aStream);
	theStream = aStream;
}

string &IBinStream::gets(string &s) {
	const int len = geti();
	Assert(len >= 0);
	if (len > 0) {
		// XXX: optimize
		char *buf = new char[len];
		get(buf, len);
		s = string(buf, len);
	} else {
		s = string();
	}
	return s;
}

///struct sockaddr_storage &IBinStream::geta(struct sockaddr_storage &a) {
///	get(&a, SizeOf(a));
///	return a;
///}

void IBinStream::get(void *buf, Size size) {
	Assert(theStream);
	theStream->read((char*)buf, size);
	//theOff += size;
}

void IBinStream::skip(Size size) {
	theStream->seekg((streamoff)size, ios::cur);
	//theOff += size;
}

///IBinStream &operator >>(IBinStream &is, NetAddr &a) {
///	struct sockaddr_storage ss;
///	is.geta(ss);
///	a = NetAddr(ss);
///	is.gets(a.rawAddrA());
///	return is;
///}


/* OBinStream */

OBinStream::OBinStream(): theStream(0) {
}

OBinStream::~OBinStream() {
}

void OBinStream::open(ostream *aStream) {
	Assert(!theStream && aStream);
	theStream = aStream;
}

void OBinStream::put(const void *buf, Size size) {
	Assert(theStream);
	theStream->write((const char*)buf, size);
	//theOff += size;
}

void OBinStream::puta(const struct sockaddr_storage &a) {
	put(&a, SizeOf(a));
}

void BinStream::open(iostream *aStream) {
	IBinStream::open(aStream);
	OBinStream::open(aStream);
}

///OBinStream &operator <<(OBinStream &os, const NetAddr &a) {
///	os.puta(a.addrN().sockAddr(a.port()));
///	os << a.rawAddrA();
///	return os;
///}
