/* (C) 2005  The Measurement Factory
   Licensed under the Apache License, Version 2.0 */

#include <xstd/config.h>

#include <errno.h>
#include <stdlib.h>

#include <iostream>
#include <cstring>

#include <xstd/TextException.h>
#include <xstd/Assert.h>


static
void Assert_checkpoint() {
	if (const char *options = getenv("XSTD_OPTIONS")) {
		if (*options == 'A') {
			cerr << "aborting to honor XSTD_OPTIONS" << endl;
			::abort();
		}
	}
}

bool Complain(const char *fname, int lineno, const char *condition) {
	cerr << fname << ':' << lineno << ": soft assertion (" <<
		(condition ? condition : "?") << ") failed" << endl;
	Assert_checkpoint();
	return false;
}


bool ComplainSys(const char *fname, int lineno) {
	cerr << fname << ':' << lineno << ": system call failure: " <<
		strerror(errno) << endl;
	Assert_checkpoint();
	return false;
}


void Abort(const char *fname, int lineno, const char *condition) {
	cerr << fname << ':' << lineno << ": assertion failed: '" 
		<< (condition ? condition : "?") << "'" << endl;
	::abort();
}

void Exit(const char *fname, int lineno, const char *condition) {
	cerr << fname << ':' << lineno << ": assertion failed: '" 
		<< (condition ? condition : "?") << "'" << endl;
	Assert_checkpoint();
	throw TextException((condition ? condition : ""), fname, lineno);
}

void Exit() {
	if (errno)
		::exit(errno);
	else
		::exit(-2);
}
