/* (C) 2005  The Measurement Factory
   Licensed under the Apache License, Version 2.0 */

#ifndef TMF_BASE_XSTD__ASSERT_H
#define TMF_BASE_XSTD__ASSERT_H

#include <xstd/config.h>

// our version of assert(3), mostly for portability purposes
#define Assert(cond) ((cond) ? (void)0 : Abort(__FILE__, __LINE__, #cond))

// same as Assert but calls Exit instead of Abort
#define Check(cond) ((cond) ? (void)0 : Exit(__FILE__, __LINE__, #cond))

// exits if cond fails
#define Must(cond) ((cond) ? (void)0 : Exit(__FILE__, __LINE__, #cond))

// logs current system error to cerr and exits if cond fails
#define MustSys(cond) ((cond) ? (void)0 : (ComplainSys(__FILE__, __LINE__), Exit()))

// logs failed cond to cerr if cond fails
#define Should(cond) ((cond) || Complain(__FILE__, __LINE__, #cond))

// logs current system error to cerr if cond fails
#define ShouldSys(cond) ((cond) || ComplainSys(__FILE__, __LINE__))


// handy for temporary debugging
#define here __FILE__ << ':' << __LINE__ << ": "

/* internal functions used by macros above */

// logs current err to cerr
extern bool Complain(const char *fname, int lineno, const char *condition);
extern bool ComplainSys(const char *fname, int lineno);

// aborts program execution and generates coredump
extern void Abort(const char *fname, int lineno, const char *condition);

// aborts program execution without coredump
extern void Exit(const char *fname, int lineno, const char *condition);
extern void Exit();

#endif
