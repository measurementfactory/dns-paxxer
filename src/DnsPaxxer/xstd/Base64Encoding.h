/* (C) 2005  The Measurement Factory
   Licensed under the Apache License, Version 2.0 */

#ifndef TMF_BASE_XSTD__BASE64ENCODING_H
#define TMF_BASE_XSTD__BASE64ENCODING_H

// #include <xstd/string.h>
#include <string>

class Base64Encoding {
	public:
		Base64Encoding(const string &data);
		
		string decode() const;
		string encode() const;

	private:
		const string theData;
};

#endif
