#ifndef DNSPAXXER_EDNS_H
#define DNSPAXXER_EDNS_H

#include <DnsPaxxer/common.h>

namespace DnsPaxxer {

class RR;

// DNS EDNS object class
class EDNS {
	public:
		friend class Packet;

		enum Flags {
			flagDO = (1 << 15)
		};

	public:
		EDNS();

		// Gets or sets the version
		uint8_t version() const {
			return theVersion;
		}

		void version(uint8_t anVersion) {
			theVersion = anVersion;
			hasInfo = true;
		}

		// Gets or sets the UDP size
		uint16_t udpSize() const {
			return theUdpSize;
		}

		void udpSize(uint16_t anUdpSize) {
			theUdpSize = anUdpSize;
			hasInfo = true;
		}

		// Gets or sets the DO flag
		bool dnssec_okay() const {
			return theDo;
		}

		void dnssec_okay(bool aDo) {
			theDo = aDo;
			hasInfo = true;
		}

		// Whether or not the object has non-default settings
		bool has_info() const {
			return hasInfo;
		}

		std::ostream& print(std::ostream& os) const;

	protected:
		void load(const RR &rr); // load info from RR
		void store(RR &rr) const; // store info to RR

	private:
		uint8_t theVersion;
		uint16_t theUdpSize;
		bool theDo;
		bool hasInfo;	// the object has some non-default settings
};

inline std::ostream &operator <<(std::ostream &os, const EDNS &edns) { return edns.print(os); }

}

#endif
