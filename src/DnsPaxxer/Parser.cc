#include <xstd/TextException.h>
#include <DnsPaxxer/Parser.h>
#include <DnsPaxxer/RRs.h>
#include <DnsPaxxer/RR.h>
#include <cstring>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/nameser.h>
#include <resolv.h>

DnsPaxxer::Parser::Parser(const char *buf, const size_type bufSize):
	theBuf(buf), theBufSize(bufSize), theBufOffset(0) {
}

DnsPaxxer::Parser::~Parser() {
}

void DnsPaxxer::Parser::parse(uint8_t & val) {
	if (theBufOffset + sizeof(uint8_t) > theBufSize)
		throw Texc("no space to parse 8bit int");
	val = theBuf[theBufOffset++];
}

uint8_t DnsPaxxer::Parser::parse8bit() {
	uint8_t raw;
	parse(raw);
	return raw;
}

void DnsPaxxer::Parser::parse(uint16_t & val) {
	uint16_t raw;
	if (theBufOffset + sizeof(raw) > theBufSize)
		throw Texc("no space to parse 16bit int");
	memcpy(&raw, theBuf + theBufOffset, sizeof(raw));
	theBufOffset += sizeof(raw);
	val = ntohs(raw);
}

uint16_t DnsPaxxer::Parser::parse16bit() {
	uint16_t raw;
	parse(raw);
	return raw;
}

uint16_t DnsPaxxer::Parser::parseRdLength() {
	return parse16bit();
}

void DnsPaxxer::Parser::parse(uint32_t & val) {
	uint32_t raw;
	if (theBufOffset + sizeof(raw) > theBufSize)
		throw Texc("no space to parse 32bit int");
	memcpy(&raw, theBuf + theBufOffset, sizeof(raw));
	theBufOffset += sizeof(raw);
	val = ntohl(raw);
}

uint32_t DnsPaxxer::Parser::parse32bit() {
	uint32_t raw;
	parse(raw);
	return raw;
}

void DnsPaxxer::Parser::parseName(std::string &name) {
	const size_type NameLenMax = 255; // per RFC 1035; XXX: declare as enum?
	char exp_domain_name [NameLenMax];

	const int compressedLen = dn_expand ( 
		(const u_char*)theBuf,
		(const u_char*)theBuf + theBufSize,
		(const u_char *)theBuf + theBufOffset,
		exp_domain_name,
		sizeof(exp_domain_name));

	if (compressedLen < 0)
		throw Texc("dn_expand(3) failure");

	theBufOffset += compressedLen;
	if (theBufOffset > theBufSize)
		throw Texc("dn_expand(3) bug");

	name = string(exp_domain_name);
}

void DnsPaxxer::Parser::parseCharString(std::string &str) {
	if (theBufOffset + 1 > theBufSize)
		throw Texc("no space to parse CharString length");

	unsigned char size = static_cast<unsigned char>(theBuf[theBufOffset++]);

	if (theBufOffset + size > theBufSize)
		throw Texc("no space to parse CharString");

	str = std::string(theBuf + theBufOffset, size);
	theBufOffset += size;
}

void DnsPaxxer::Parser::parseRawData(std::string &data, const uint16_t size) {
	if (theBufOffset + size > theBufSize)
		throw Texc("no space to parse RawData");

	data = std::string(theBuf + theBufOffset, size);
	theBufOffset += size;
}
