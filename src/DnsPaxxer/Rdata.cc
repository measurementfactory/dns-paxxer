#include <DnsPaxxer/Packer.h>
#include <DnsPaxxer/Parser.h>
#include <DnsPaxxer/Packet.h>
#include <DnsPaxxer/Rdata.h>
#include <DnsPaxxer/RR.h>
#include <xstd/TextException.h>
#include <xstd/BinStream.h>
#include <xstd/FxStream.h>
#include <xstd/Base64Encoding.h>

#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <ctime>

DnsPaxxer::Rdata::~Rdata() {
}

DnsPaxxer::Rdata::Rdata() {
}

std::string
DnsPaxxer::Rdata::string() const {
	char buf[65535]; // RDLENGTH field limits RDATA size to 64KB
	OFixedStream ofs(buf, sizeof(buf));
	print(ofs);
	return std::string(buf, ofs.tellp());
}

// ===========================================================================

DnsPaxxer::RawRdata::RawRdata() {
}

DnsPaxxer::RawRdata::RawRdata(const std::string &raw) : theRaw(raw) {
}

void
DnsPaxxer::RawRdata::clear() {
	theRaw.clear();
}

void
DnsPaxxer::RawRdata::print(ostream & os) const {
	os << "\\# " << theRaw.size() << ' ' << HexBytesString(theRaw);
}

void
DnsPaxxer::RawRdata::pack(Packer & packer) const {
	const uint16_t size = static_cast<uint16_t>(theRaw.size());
	packer.pack(size); // RDLENGTH
	packer.packRawData(theRaw, size);
}

void
DnsPaxxer::RawRdata::parse(Parser & parser) {
	const uint16_t size = parser.parseRdLength(); // RDLENGTH
	parser.parseRawData(theRaw, size);
}


// ===========================================================================

DnsPaxxer::NameRdata::NameRdata() {
}

DnsPaxxer::NameRdata::NameRdata(const std::string &name) : theName(name) {
}

void
DnsPaxxer::NameRdata::clear() {
	theName.clear();
}

void
DnsPaxxer::NameRdata::print(std::ostream & os) const {
	os << theName;
}

void
DnsPaxxer::NameRdata::parse(Parser & parser) {
	(void)parser.parseRdLength();
	parser.parseName(theName);
}

void
DnsPaxxer::NameRdata::pack(Packer & packer) const {
	const size_t off = packer.skipLengthField();
	packer.packName(theName);
	packer.bakeLengthField(off);
}

// ===========================================================================

DnsPaxxer::CharStrRdata::CharStrRdata() {
}

DnsPaxxer::CharStrRdata::CharStrRdata(const std::string &charstr) : theCharStr(charstr) {
}

void
DnsPaxxer::CharStrRdata::clear() {
	theCharStr.clear();
}

void
DnsPaxxer::CharStrRdata::print(ostream & os) const {
	os << theCharStr;
}

void
DnsPaxxer::CharStrRdata::parse(Parser & parser) {
	(void)parser.parseRdLength();
	parser.parseCharString(theCharStr);
}

void 
DnsPaxxer::CharStrRdata::pack(Packer & packer) const {
	packer.pack(static_cast<uint16_t>(theCharStr.length() + 1));
    packer.packCharString(theCharStr);
}


// ===========================================================================
// RR-specific classes below

// ===========================================================================

DnsPaxxer::ARdata::ARdata() : RawRdata() {
}

DnsPaxxer::ARdata::ARdata(const std::string &addr) {
	theRaw.resize(4);
        inet_pton(AF_INET, addr.c_str(), (void*) theRaw.data());
}

void
DnsPaxxer::ARdata::print(ostream & os) const {
	char buf[20];
	inet_ntop(AF_INET, theRaw.data(), buf, sizeof(buf));
	buf[sizeof(buf)-1] = '\0';
	os << buf;
}

// ===========================================================================

DnsPaxxer::AaaaRdata::AaaaRdata() : RawRdata() {
}

DnsPaxxer::AaaaRdata::AaaaRdata(const std::string &addr) {
	theRaw.resize(16);
        inet_pton(AF_INET6, addr.c_str(), (void*) theRaw.data());
}

void
DnsPaxxer::AaaaRdata::print(ostream & os) const {
	char buf[64];
	inet_ntop(AF_INET6, theRaw.data(), buf, sizeof(buf));
	buf[sizeof(buf)-1] = '\0';
	os << buf;
}

// ===========================================================================

DnsPaxxer::SoaRdata::SoaRdata() : theSerial(0), theRefresh(0), theRetry(0), theExpire(0), theMinimum(0) {
}

DnsPaxxer::SoaRdata::SoaRdata(const std::string &mname, const std::string &rname, uint32_t serial, uint32_t refresh, uint32_t retry, uint32_t expire, uint32_t minimum) : theMname(mname), theRname(rname), theSerial(serial), theRefresh(refresh), theRetry(retry), theExpire(expire), theMinimum(minimum) {
}

void
DnsPaxxer::SoaRdata::clear() {
	theMname.clear();
	theRname.clear();
	theSerial = 0;
	theRetry = 0;
	theRefresh = 0;
	theExpire = 0;
	theMinimum = 0;
}

void
DnsPaxxer::SoaRdata::print(ostream & os) const {
	os <<
		theMname << ' ' <<
		theRname << ' ' <<
		theSerial << ' ' << theRetry << ' ' <<
		theRefresh << ' ' << theExpire << ' ' << theMinimum;
}

void
DnsPaxxer::SoaRdata::parse(Parser & parser) {
	(void)parser.parseRdLength();

	parser.parseName(theMname);
	parser.parseName(theRname);

	parser.parse(theSerial);
	parser.parse(theRetry);
	parser.parse(theRefresh);
	parser.parse(theExpire);
	parser.parse(theMinimum);
}

void 
DnsPaxxer::SoaRdata::pack(Packer & packer) const {
	const size_t off = packer.skipLengthField();
	packer.packName(theMname);
	packer.packName(theRname);

	packer.pack(theSerial);
	packer.pack(theRetry);
	packer.pack(theRefresh);
	packer.pack(theExpire);
	packer.pack(theMinimum);
	packer.bakeLengthField(off);
}

// ===========================================================================

DnsPaxxer::CnameRdata::CnameRdata() : NameRdata() {
}

DnsPaxxer::CnameRdata::CnameRdata(const std::string &cname) : NameRdata(cname) {
}

// ===========================================================================

DnsPaxxer::TxtRdata::TxtRdata() : CharStrRdata() {
}

DnsPaxxer::TxtRdata::TxtRdata(const std::string &txt) : CharStrRdata(txt) {
}

// ===========================================================================

DnsPaxxer::MxRdata::MxRdata() : thePref(0) {
}

DnsPaxxer::MxRdata::MxRdata(uint16_t pref, const std::string &exchange) : thePref(pref), theExchange(exchange) {
}

void
DnsPaxxer::MxRdata::clear() {
	thePref = 0;
	theExchange.clear();
}

void
DnsPaxxer::MxRdata::print(ostream & os) const {
	os <<
		thePref << ' ' <<
		theExchange;
}

void
DnsPaxxer::MxRdata::parse(Parser & parser) {
	(void)parser.parseRdLength();
	parser.parse (thePref );
	parser.parseName(theExchange);
}

void
DnsPaxxer::MxRdata::pack(Packer & packer) const {
	const size_t off = packer.skipLengthField();
	packer.pack(thePref);
	packer.packName(theExchange);
	packer.bakeLengthField(off);
}

// ===========================================================================

DnsPaxxer::RrSigRdata::RrSigRdata():
		theTypeCovered(0), theAlgorithm(0), theLabels(0),theTtl(0), theExpiration(0), theInception(0), theKeyTag(0) {
}

DnsPaxxer::RrSigRdata::RrSigRdata(uint16_t type, uint8_t algorithm, uint8_t labels,	uint32_t ttl, uint32_t expiration, uint32_t inception, uint16_t keytag, const std::string & signer, const std::string & signature):
		theTypeCovered(type), theAlgorithm(algorithm), theLabels(labels), theTtl(ttl), theExpiration(expiration), theInception(inception), theKeyTag(keytag), theSignerName(signer), theSignature(signature) {
}

void
DnsPaxxer::RrSigRdata::clear() {
	theTypeCovered = 0;
	theAlgorithm = 0;
	theLabels =	0;
	theTtl = 0;
	theExpiration = 0;
	theInception = 0;
	theKeyTag = 0;
	theSignerName.clear();
	theSignature.clear();
}

void
DnsPaxxer::RrSigRdata::print(std::ostream & os) const {
	Base64Encoding b64signature(theSignature);
	const time_t expTime = theExpiration;
	const time_t incTime = theInception;
	tm expTm, incTm;

	if (!gmtime_r(&expTime, &expTm))
		throw Texc("wrong signature expiration time");
	if (!gmtime_r(&incTime, &incTm))
		throw Texc("wrong signature inception time");

	os << RR::TypeString(theTypeCovered) << ' ' <<
		static_cast<uint16_t>(theAlgorithm) << ' ' <<
		static_cast<uint16_t>(theLabels) << ' ' <<
		theTtl << ' ' <<
		std::setfill('0') <<
			(expTm.tm_year+1900) <<
			std::setw(2) << (expTm.tm_mon+1) <<
			std::setw(2) << expTm.tm_mday <<
			std::setw(2) << expTm.tm_hour <<
			std::setw(2) << expTm.tm_min <<
			std::setw(2) << expTm.tm_sec <<
			' ' <<
			(incTm.tm_year+1900) << 
			std::setw(2) << (incTm.tm_mon+1) <<
			std::setw(2) << incTm.tm_mday <<
			std::setw(2) << incTm.tm_hour <<
			std::setw(2) << incTm.tm_min <<
			std::setw(2) << incTm.tm_sec <<
			' ' <<
		std::setfill(' ') <<
		theKeyTag << ' ' <<
		theSignerName << '.' << ' ' <<
		b64signature.encode();
}

void
DnsPaxxer::RrSigRdata::pack(Packer & packer) const {
	const size_t off = packer.skipLengthField();

	packer.pack(theTypeCovered);
	packer.pack(theAlgorithm);
	packer.pack(theLabels);
	packer.pack(theTtl);
	packer.pack(theExpiration);
	packer.pack(theInception);
	packer.pack(theKeyTag);

	packer.packName(theSignerName, false); // DNS name compression MUST NOT be used
	packer.packRawData(theSignature, theSignature.size());

	packer.bakeLengthField(off);
}

void
DnsPaxxer::RrSigRdata::parse(Parser & parser) {
	const uint16_t size = parser.parseRdLength();
	const Parser::size_type rdStart = parser.offset();

	parser.parse(theTypeCovered);
	parser.parse(theAlgorithm);
	parser.parse(theLabels);
	parser.parse(theTtl);
	parser.parse(theExpiration);
	parser.parse(theInception);
	parser.parse(theKeyTag);

	parser.parseName(theSignerName);
	parser.parseRawData(theSignature, size - (parser.offset() - rdStart));
}

// ===========================================================================

DnsPaxxer::DnsKeyRdata::DnsKeyRdata():
		theFlags(0), theProtocol(0), theAlgorithm(0) {
}

DnsPaxxer::DnsKeyRdata::DnsKeyRdata(uint16_t flags, uint8_t protocol, uint8_t algorithm, const std::string & key):
		theFlags(flags), theProtocol(protocol), theAlgorithm(algorithm), theKey(key) {
}

void
DnsPaxxer::DnsKeyRdata::clear() {
	theFlags = 0;
	theProtocol = 0;
	theAlgorithm = 0;
	theKey.clear();
}

void
DnsPaxxer::DnsKeyRdata::print(std::ostream & os) const {
	Base64Encoding b64key(theKey);

	os << theFlags << ' ' <<
		static_cast<uint16_t>(theProtocol) << ' ' <<
		static_cast<uint16_t>(theAlgorithm) << ' ' <<
		b64key.encode();
}

void
DnsPaxxer::DnsKeyRdata::pack(Packer & packer) const {
	const size_t off = packer.skipLengthField();

	packer.pack(theFlags);
	packer.pack(theProtocol);
	packer.pack(theAlgorithm);
	packer.packRawData(theKey, theKey.size());

	packer.bakeLengthField(off);
}

void
DnsPaxxer::DnsKeyRdata::parse(Parser & parser) {
	const uint16_t size = parser.parseRdLength();
	const Parser::size_type rdStart = parser.offset();

	parser.parse(theFlags);
	parser.parse(theProtocol);
	parser.parse(theAlgorithm);
	parser.parseRawData(theKey, size - (parser.offset() - rdStart));
}

// ===========================================================================

DnsPaxxer::DsRdata::DsRdata():
		theKeyTag(0), theAlgorithm(0), theDigestType(0) {
}

DnsPaxxer::DsRdata::DsRdata(uint16_t key, uint8_t algorithm, uint8_t type, const std::string & digest):
		theKeyTag(key), theAlgorithm(algorithm), theDigestType(type), theDigest(digest) {
}

void
DnsPaxxer::DsRdata::clear() {
	theKeyTag = 0;
	theAlgorithm = 0;
	theDigestType = 0;
	theDigest.clear();
}

void
DnsPaxxer::DsRdata::print(std::ostream & os) const {
	os << theKeyTag << ' ' <<
		static_cast<uint16_t>(theAlgorithm) << ' ' <<
		static_cast<uint16_t>(theDigestType) << ' ' <<
		HexBytesString(theDigest);
}

void
DnsPaxxer::DsRdata::pack(Packer & packer) const {
	const size_t off = packer.skipLengthField();

	packer.pack(theKeyTag);
	packer.pack(theAlgorithm);
	packer.pack(theDigestType);
	packer.packRawData(theDigest, theDigest.size());

	packer.bakeLengthField(off);
}

void
DnsPaxxer::DsRdata::parse(Parser & parser) {
	const uint16_t size = parser.parseRdLength();
	const Parser::size_type rdStart = parser.offset();

	parser.parse(theKeyTag);
	parser.parse(theAlgorithm);
	parser.parse(theDigestType);
	parser.parseRawData(theDigest, size - (parser.offset() - rdStart));
}

// ===========================================================================

std::ostream& DnsPaxxer::HexBytesString::print(std::ostream& os) const {
	os << std::hex << std::setfill('0');
	for (std::string::const_iterator it=bytes.begin(); it!=bytes.end(); ++it)
		os << std::setw(2) << static_cast<uint16_t>(static_cast<uint8_t>(*it));
	os << std::setfill(' ') << std::dec;
	return os;
}
