#ifndef DNSPAXXER_HEADER_H
#define DNSPAXXER_HEADER_H

#include <DnsPaxxer/common.h>

namespace DnsPaxxer {

class Packer;
class Parser;

// DNS header object class
class Header {
	public:
		friend class Packet;

		enum QR {
			qrMask = (1 << 15)
		};

		static const unsigned int opcodeShift = 11;
		enum OpCodes {
			opcodeMask = 0x7800,
			opcodeStdQuery = 0,
			opcodeInvQuery = 1,
			opcodeSrvStat = 2,
			opcodeReserved3 = 3,
			opcodeNotify = 4,
			opcodeUpdate = 5,
			opcodeReserved6 = 6,
			opcodeReserved7 = 7,
			opcodeReserved8 = 8,
			opcodeReserved9 = 9,
			opcodeReserved10 = 10,
			opcodeReserved11 = 11,
			opcodeReserved12 = 12,
			opcodeReserved13 = 13,
			opcodeReserved14 = 14,
			opcodeReserved15 = 15
		};

		enum RCodes	{
			rcodeMask = 0x07,
			rcodeNoError = 0,
			rcodeFmt = 1,
			rcodeSrvFail,
			rcodeNoName,
			rcodeNoSupp,
			rcodeRefused
		};

		enum Flags {
			flagAA = (1 << 10),
			flagTC = (1 << 9),
			flagRD = (1 << 8),
			flagRA = (1 << 7),
			flagAD = (1 << 5),
			flagCD = (1 << 4)
		};

	public:
		Header();

		void clear();

		void pack(Packer & packer) const;
		void parse(Parser & parser);

		// Gets or sets the query identification number
		uint16_t id() const {
			return theId;
		}

		void id(uint16_t anId) {
			theId = anId;
		}

		// Gets or sets the query response flag
		bool qr() const {
			return theQr;
		}

		void qr(bool aQr) {
			theQr = aQr;
		}

		// Gets or sets the query opcode (the purpose of the query)
		OpCodes opcode() const {
			return theOpCode;
		}

		void opcode(OpCodes anOpCode) {
			theOpCode = anOpCode;
		}

		// Gets or sets the authoritative answer flag
		bool aa() const {
			return theAa;
		}

		void aa(bool anAa) {
			theAa = anAa;
		}

		// Gets or sets the truncated packet flag
		bool tc() const {
			return theTc;
		}

		void tc(bool aTc) {
			theTc = aTc;
		}

		// Gets or sets the recursion desired flag
		bool rd() const {
			return theRd;
		}

		void rd(bool anRd) {
			theRd = anRd;
		}

		// Gets or sets the checking disabled flag
		bool cd() const {
			return theCd;
		}

		void cd(bool aCd) {
			theCd = aCd;
		}

		// Gets or sets the recursion avaiable flag
		bool ra() const {
			return theRa;
		}

		void ra(bool aRa) {
			theRa = aRa;
		}

		// Gets or sets the authentic data flag
		bool ad() const {
			return theAd;
		}

		void ad(bool aAd) {
			theAd = aAd;
		}

		// Gets or sets the query response code (the status of the query)
		RCodes rcode() const {
			return theRcode;
		}

		void rcode(RCodes aRCode) {
			theRcode = aRCode;
		}

		// Gets the expected number of questions records; should be sync-ed with Packet::theQuestions.size()
		uint16_t qdcount() const {
			return theQdCount;
		}

		// Gets the expected number of answers records; should be sync-ed with Packet::theAnswers.size()
		uint16_t ancount() const {
			return theAnCount;
		}

		// Gets the expected number of authority records; should be sync-ed with Packet::theAuthorities.size()
		uint16_t nscount() const {
			return theNsCount;
		}

		// Gets the expected number of additional records; should be sync-ed with Packet::theAdditions.size()
		uint16_t arcount() const {
			return theArCount;
		}

		std::ostream& print(std::ostream& os) const;

	protected:
		OpCodes theOpCode;
		RCodes theRcode;
		uint16_t theId;
		uint16_t theQdCount, theAnCount, theNsCount, theArCount;
		bool theQr;
		bool theAa, theTc, theRd, theCd, theRa, theAd;
};

inline std::ostream &operator <<(std::ostream &os, const Header &hdr) { return hdr.print(os); }

}

#endif
